
#==================================ipa==================================
echo "开始打包ipa"
flutter build ios --release #--no-codesign

if [ -d build/ios/iphoneos/Runner.app ]
    then

    mkdir app/Payload

    cp -r build/ios/iphoneos/Runner.app app/Payload

    cd app
    zip -r -m ios-$(date "+%Y%m%d%H%M").ipa Payload
    cd ..

    echo "打包很顺利😄"
    say "打包成功"

    open app

else
    echo "遇到报错了😭, 打开Xcode查找错误原因"
    say "打包失败"
fi
##==================================ipa==================================

echo "📅  Finished. Elapsed time: ${SECONDS}s"

