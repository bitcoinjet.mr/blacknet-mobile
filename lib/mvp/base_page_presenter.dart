import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

import 'package:blacknet/net/net.dart';

import 'IView.dart';
import 'IPresenter.dart';

class BasePagePresenter<V extends IView> extends IPresenter {

  V view;

  CancelToken _cancelToken;

  BasePagePresenter() {
    _cancelToken = CancelToken();
  }
  
  @override
  void deactivate() {}

  @override
  void didChangeDependencies() {}

  @override
  void didUpdateWidgets<W>(W oldWidget) {}

  @override
  void dispose() {
    /// 销毁时，将请求取消
    if (!_cancelToken.isCancelled) {
      _cancelToken.cancel();
    }
  }

  @override
  void initState() {}

  /// 返回Future 适用于刷新，加载更多
  Future request<T>(Method method, {@required String url, bool isShow : true, bool isClose: true, Function(GeneralResponse t) onSuccess,
    Function(int code, String msg) onError, dynamic params, 
    Map<String, dynamic> queryParameters, CancelToken cancelToken, Options options, bool isList : false}) {
    if (isShow) view.showProgress();
    return DioUtils.instance.requestNetwork(method, url,
        params: params,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken?? _cancelToken,
        onSuccess: (data) {
          if (isClose) view.closeProgress();
          if (onSuccess != null) {
            onSuccess(data);
          }
        },
        onError: (code, msg) {
          _onError(code, msg, onError);
        }
    );
  }

  void asyncRequestNetwork<T>(Method method, {@required String url, bool isShow : true, bool isClose: true, Function(GeneralResponse t) onSuccess, Function(int code, String msg) onError,
    dynamic params, Map<String, dynamic> queryParameters, CancelToken cancelToken, Options options, bool isList : false}) {
    if (isShow) view.showProgress();
    DioUtils.instance.asyncRequestNetwork(method, url,
        params: params,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken?? _cancelToken,
        isList: isList,
        onSuccess: (data) {
          if (isClose) view.closeProgress();
          if (onSuccess != null) {
            onSuccess(data);
          }
        },
        onError: (code, msg) {
          _onError(code, msg, onError);
        }
    );
  }


  _onError(int code, String msg, Function(int code, String msg) onError) {
    /// 异常时直接关闭加载圈，不受isClose影响
    view.closeProgress();
    if (code != ExceptionHandle.cancel_error) {
      view.showToast(msg);
    }
    /// 页面如果dispose，则不回调onError
    if (onError != null && view.getContext() != null) {
      onError(code, msg);
    }
  } 
}