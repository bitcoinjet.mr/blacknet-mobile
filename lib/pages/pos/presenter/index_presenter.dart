import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/mvp/mvp.dart';
import 'package:blacknet/pages/pos/index.dart';

class PoSPagePresenter extends BasePagePresenter<PoSPageState> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async{
      view.showProgress();
      try {
        await Provider.of<DataCenterProvider>(view.context).refreshPoSState(); 
      } catch (e) {
        print(e);
      }
      view.closeProgress();
    });

  }
}
