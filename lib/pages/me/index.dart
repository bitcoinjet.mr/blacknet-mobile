import 'dart:async';
import 'dart:ui' as ui;
import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:package_info/package_info.dart';

import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/pages/me/presenter/index_presenter.dart';

class MePage extends StatefulWidget {
  @override
  MePageState createState() => MePageState();
}

class MePageState extends BasePageState<MePage, MePageStatePresenter>
    with AutomaticKeepAliveClientMixin<MePage>, SingleTickerProviderStateMixin {

  @override
  MePageStatePresenter createPresenter() {
    return MePageStatePresenter();
  }

  @override
  bool get wantKeepAlive => false;

  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  final LocalAuthenticationService _localAuth = locator<LocalAuthenticationService>();

  ScrollController _controller = new ScrollController();

  bool _hideDivider = false;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      setState(() {
        _hideDivider = _controller.offset > 5;
      });
    });
  }

  @override
  void dispose() {
    _verificationNotifier.close();
    _controller.dispose();
    super.dispose();
  }

  Widget i18n(languageModel) {
    return new ListTile(
      title: Text(S.of(context).language),
      leading: new CircleAvatar(
          child: new Icon(Icons.language),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(S.of(context).lang(languageModel.titleId)),
          Icon(Icons.keyboard_arrow_right)
        ],
      ),
      onTap: () {
        OkNavigator.push(context, Routes.language);
      },
    );
  }

  Widget get about {
    return new ListTile(
      title: Text(S.of(context).about),
      leading: new CircleAvatar(
          child: new Text("Ab"),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      trailing: FutureBuilder(
        future: getVersionNumber(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) =>
            Text(snapshot.hasData ? snapshot.data : " ..."),
      ),
      onTap: () {
        OkNavigator.push(context, Routes.about);
      },
    );
  }

  void logoutAction() async {
    var close = await showDialog<bool>(
      context: context,
      barrierDismissible: true, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).logoutTitle),
          actions: <Widget>[
            FlatButton(
              child: Text(S.of(context).cancel),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text(S.of(context).logout),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    );
    if (close != null && close) {
      Provider.of<AccountProvider>(context).setCancelLease([]);
      Provider.of<AccountProvider>(context).setBalance(BlnBalance.empty());
      Provider.of<AccountProvider>(context).setCurrentBln(Bln.empty());
      OkNavigator.push(context, Routes.index, replace: true, clearStack: true);
    }
  }

  Widget get logout {
    return ListTile(
      title: Text(S.of(context).logout),
      leading: new CircleAvatar(
          child: new Icon(Icons.exit_to_app),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: logoutAction,
      enabled: true,
    );
  }

  Future<void> checkPasswordAndShowCode(context) async{
    if(await showBottomAuth()){
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      var mnemonic = await bln.getMnemonic();
      OkNavigator.push(context, '${Routes.accountBackup}?address=${bln.address}&mnemonic=${mnemonic}');
    }
  }

  Widget backupMnemonicCode(context) {
    return ListTile(
      title: Text(S.of(context).backupMnemonic),
      leading: new CircleAvatar(
          child: new Icon(Icons.lock),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: () async{
        await checkPasswordAndShowCode(context);
      },
      enabled: true,
    );
  }

  Widget switchSecretFreePayment() {
    return ListTile(
      title: Text(S.of(context).secretFreePayment),
      leading: new CircleAvatar(
          child: new Icon(Icons.security),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: (){
        OkNavigator.push(context, Routes.switchAuth);
      },
      enabled: true,
    );
  }
  
  Widget paymentManagement(context) {
    return ListTile(
      title: Text(S.of(context).paymentManagement),
      leading: new CircleAvatar(
          child: new Icon(Icons.lock),
          foregroundColor: Theme.of(context).accentColor,
          backgroundColor: Theme.of(context).canvasColor),
      onTap: () {

      },
      enabled: true,
    );
  }

  Future<ui.Image> _loadOverlayImage() async {
    final completer = Completer<ui.Image>();
    final byteData = await rootBundle.load('assets/images/logo.png');
    ui.decodeImageFromList(byteData.buffer.asUint8List(), completer.complete);
    return completer.future;
  }

  Future<String> getVersionNumber() async {
    
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;
    return version;
  }

  @override
  Widget build(BuildContext context) {
    Locale l = Provider.of<GlobalProvider>(context).getLanguage();
    LanguageModel languageModel = LanguageModel(S.delegate.title(l),
        l.languageCode, l.countryCode, S.delegate.getLiteralText(l));
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    if (bln.isEmpty()) {
      return Scaffold();
    }
    return Scaffold(
      body: CustomScrollView(
        controller: _controller,
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            delegate: _SliverAppBarDelegate(rightOnTap: (){
              return showQRcode();
            }),
          ),
          SliverFillRemaining(
            child: ListView(
              // controller: _controller,
              padding: EdgeInsets.zero,
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                ListTile(
                    contentPadding: EdgeInsets.only(top: 20, bottom: 15, left: 20, right: 20),
                    title: Text(bln.name,
                        style: TextStyle(
                            fontSize: 20,
                            color:
                                Theme.of(context).appBarTheme.textTheme.title.color)),
                    subtitle: Text(bln.address,
                        style: TextStyle(
                            color:
                                Theme.of(context).appBarTheme.textTheme.title.color)),
                    // trailing: new Icon(Icons.content_copy, size: 16),
                    onTap: () {
                      Clipboard.setData(new ClipboardData(text: bln.address)).then((v) {
                        Toast.show(S.of(context).copySuccess);
                      });
                    },
                    isThreeLine: false,
                    leading: new CircleAvatar(
                      backgroundImage: AssetImage('assets/images/1024.png'),
                    )
                  ),
                _hideDivider ? Divider(color: Theme.of(context).canvasColor) : Divider(),
                backupMnemonicCode(context),
                switchSecretFreePayment(),
                i18n(languageModel),
                about,
                logout
              ],
            ),
          ),
        ],
      )
    );
  }
}


class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    this.rightOnTap
  });
  final double minHeight = 100;
  final double maxHeight = 100;
  final Function rightOnTap;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
        Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
        bool hideDivider = shrinkOffset > 5;
        return AppBar(
          backgroundColor: Theme.of(context).canvasColor,
          title: hideDivider ? ListTile(
            title: Text(bln.name),
            // subtitle: Text(shortAddress(bln.address, 15)),
            leading: new CircleAvatar(
              backgroundImage: AssetImage('assets/images/1024.png'),
            )
          ) : null,
          titleSpacing: 0,
          automaticallyImplyLeading:false,
          elevation: hideDivider ? 5 : 0,
          actions: <Widget>[
            IconButton( 
              icon: new Icon(Icons.apps, color: Theme.of(context).accentColor),
              onPressed: rightOnTap,
            )
          ],
        );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight;
    // return maxHeight != oldDelegate.maxHeight ||
    //     minHeight != oldDelegate.minHeight ||
    //     child != oldDelegate.child;
  }
}
