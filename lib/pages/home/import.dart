import 'package:blacknet/models/index.dart';
import 'package:blacknet/provider/account.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/routers/routers.dart';
// theme
import 'package:blacknet/theme/index.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';

// widgets
import 'package:blacknet/widgets/index.dart';
import 'package:provider/provider.dart';

class ImportPage extends StatefulWidget {
  @override
  _ImportPage createState() => _ImportPage();
}

class _ImportPage extends State<ImportPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Expanded(
              flex: 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: ThemeLayout.width(160),
                    height: ThemeLayout.width(160),
                    child: new Image.asset(
                      'assets/images/logo.png',
                    )
                  ),
                  Container(
                    margin: ThemeLayout.marginTop(20),
                    child: new Text(S.of(context).name, style: new TextStyle(
                      fontSize: ThemeLayout.fontSize(24),
                    ))
                  )
                ],
              )
            ),
            new Expanded(
              flex: 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Expanded(
                    flex: 2,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: ThemeLayout.width(300),
                          padding: ThemeLayout.edgeInsetsAll(10),
                          child: MyButton(
                            key: Key("import"),
                            text: S.of(context).importWallet,
                            onPressed: () async {
                              OkNavigator.push(context, Routes.accountImport1);
                            }
                          )
                        ),
                        SizedBox(height: ThemeLayout.height(20)),
                        Container(
                          width: ThemeLayout.width(300),
                          padding: ThemeLayout.edgeInsetsAll(10.0),
                          child: MyButton(
                            key: Key("create"),
                            text: S.of(context).craeteWallet,
                            onPressed: () async {
                              Bln bln = new Bln();
                              await bln.saveMnemonic();
                              var mnemonic = await bln.getMnemonic();
                              Provider.of<AccountProvider>(context).setCurrentBln(bln);
                              OkNavigator.push(context, '${Routes.accountBackup}?address=${bln.address}&mnemonic=${mnemonic}');
                            }
                          )
                        )
                      ],
                    )
                  )
                ],
              ),
            )
          ]
        )
      )
    );
  }
}