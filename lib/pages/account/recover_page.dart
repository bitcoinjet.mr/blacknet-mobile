import 'package:blacknet/provider/account.dart';
import 'package:blacknet/theme/index.dart';
import 'package:blacknet/widgets/index.dart';
import 'package:flutter/material.dart';
// i8n
import 'package:blacknet/generated/i18n.dart';
// router
import 'package:blacknet/routers/routers.dart';
// utils
import 'package:blacknet/utils/index.dart';
// model
import 'package:blacknet/models/index.dart';
import 'package:provider/provider.dart';

class AccountRecoverPage extends StatefulWidget {
  @override
  _AccountRecoverPage createState() => _AccountRecoverPage();
}

class _AccountRecoverPage extends State<AccountRecoverPage> {
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  Bln bln = Bln.empty();
  @override
  void initState() {
    super.initState();
  }

  void importSeed() async{
      var _form = _formKey.currentState;
      if (_form.validate() && _validate) {
        _form.save();
        bln.address = bln.generateAddress();
        await bln.saveMnemonic();
        Provider.of<AccountProvider>(context).setCurrentBln(bln);
        OkNavigator.push(context, Routes.index, clearStack: true);
      }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            title: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(S.of(context).importWallet)
              ],
            )),
        body: MyPage(
            child: ListView(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Card(
                          margin: const EdgeInsets.only(top: 25),
                          elevation: 3,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                  color: Theme.of(context).canvasColor,
                                  child: ListTile(
                                      title: Container(
                                          padding:
                                              const EdgeInsets.only(top: 10),
                                          child: Text(S
                                              .of(context)
                                              .importInputMnemonic)),
                                      subtitle: Container(
                                        padding: const EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Text(S
                                            .of(context)
                                            .importInputMnemonicDesc),
                                      ))),
                              TextFormField(
                                  autofocus: false,
                                  decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.all(10),
                                      border: InputBorder.none,
                                      focusedBorder:
                                          !_validate && bln.mnemonic != null
                                              ? OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.redAccent,
                                                      width: 0.8))
                                              : InputBorder.none),
                                  maxLines: 6,
                                  onSaved: (val) {
                                    bln.mnemonic = val.trim();
                                  },
                                  onChanged: (val) {
                                    var v = val.split(' ');
                                    if (v.length == 12) {
                                      setState(() {
                                        bln.mnemonic = val.trim();
                                        _validate = true;
                                      });
                                    } else {
                                      setState(() {
                                        bln.mnemonic = val;
                                        _validate = false;
                                      });
                                    }
                                  },
                                  autovalidate: true,
                                  validator: (value) {
                                    return null;
                                  })
                            ],
                          ),
                        ),
                        !_validate && bln.mnemonic != null
                            ? Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Text(S.of(context).importCorrectMnemonic,
                                    style: TextStyle(
                                        color: Theme.of(context).errorColor)),
                              )
                            : SizedBox(),
                        Container(
                            margin: const EdgeInsets.only(top: 25),
                            child: MyButton(
                                key: Key("import"),
                                text: S.of(context).nextStep,
                                onPressed: !_validate
                                    ? null
                                    : importSeed)
                                  )
                      ],
                    )))
          ],
        )));
  }

  Future<Bln> createBln() async {
    await Future.delayed(Duration(milliseconds: 500));
    Bln bln = new Bln();
    return bln;
  }
}
