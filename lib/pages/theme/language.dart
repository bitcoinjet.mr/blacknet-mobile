import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:blacknet/provider/global.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// theme
import 'package:blacknet/theme/index.dart';
// models
import 'package:blacknet/models/index.dart';

class LanguagePage extends StatefulWidget {

  @override
  _LanguagePageState createState() => _LanguagePageState();

}

class _LanguagePageState extends State<LanguagePage> {

  final List<Color> colors = ThemeColor.supportColors;

  List<LanguageModel> langs = new List();

  LanguageModel _currentLanguage;

  @override
  void initState() {
    super.initState();
    S.delegate.supportedLocales.forEach((l){
      langs.add(LanguageModel(S.delegate.title(l), l.languageCode, l.countryCode, S.delegate.getLiteralText(l)));
    });
  }

  void _updateData() {
    String language = _currentLanguage.countryCode;
    for (int i = 0, length = langs.length; i < length; i++) {
      langs[i].isSelected = (langs[i].countryCode == language);
    }
  }

  @override
  Widget build(BuildContext context) {
    Locale l = Provider.of<GlobalProvider>(context).getLanguage();
    _currentLanguage = LanguageModel(S.delegate.title(l), l.languageCode,l.countryCode, S.delegate.getLiteralText(l));
    _updateData();
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).language),
        centerTitle: true,
        // iconTheme: IconThemeData(color: Colors.white),
      ),
      body: new ListView.builder(
        itemCount: langs.length,
        itemBuilder: (BuildContext context, int index) {
          LanguageModel model = langs[index];
          return new ListTile(
            title: new Text( model.literalText ),
            trailing: new Radio(
                value: true,
                groupValue: model.isSelected == true,
                activeColor: Theme.of(context).accentColor
            ),
            onTap: () {
              if(_currentLanguage.titleId != model.titleId){
                Locale locale = new Locale(model.languageCode, model.countryCode);
                Provider.of<GlobalProvider>(context).setLanguage(locale);
              }
            },
          );
        }),
    );
  }

}