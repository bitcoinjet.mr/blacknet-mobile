class LanguageModel {
  String titleId;
  String languageCode;
  String countryCode;
  String literalText;
  bool isSelected;

  LanguageModel(this.titleId, this.languageCode, this.countryCode, this.literalText,
      {this.isSelected: false});

  LanguageModel.fromJson(Map<String, dynamic> json)
      : titleId = json['titleId'],
        languageCode = json['languageCode'],
        countryCode = json['countryCode'],
        literalText = json['literalText'],
        isSelected = json['isSelected'];

  Map<String, dynamic> toJson() => {
        'titleId': titleId,
        'languageCode': languageCode,
        'countryCode': countryCode,
        'literalText': literalText,
        'isSelected': isSelected,
      };

  @override
  String toString() {
    StringBuffer sb = new StringBuffer('{');
    sb.write("\"titleId\":\"$titleId\"");
    sb.write(",\"languageCode\":\"$languageCode\"");
    sb.write(",\"countryCode\":\"$countryCode\"");
    sb.write(",\"literalText\":\"$literalText\"");
    sb.write('}');
    return sb.toString();
  }
}