import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class ThemeLayout {
    static EdgeInsets edgeInsetsAll(double value) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(value.toInt()),
        right: ScreenUtil().setWidth(value.toInt()),
        top: ScreenUtil().setHeight(value.toInt()),
        bottom: ScreenUtil().setHeight(value.toInt())
      );
    }
    static EdgeInsets all(double value) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(value.toInt()),
        right: ScreenUtil().setWidth(value.toInt()),
        top: ScreenUtil().setHeight(value.toInt()),
        bottom: ScreenUtil().setHeight(value.toInt())
      );
    }
    static EdgeInsets top(double value) { 
      return EdgeInsets.only(
        top: ScreenUtil().setHeight(value.toInt())
      );
    }
    static EdgeInsets bottom(double value) { 
      return EdgeInsets.only(
        bottom: ScreenUtil().setHeight(value.toInt())
      );
    }
    static EdgeInsets left(double value) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(value.toInt())
      );
    }
    static EdgeInsets right(double value) { 
      return EdgeInsets.only(
        right: ScreenUtil().setWidth(value.toInt())
      );
    }
    static EdgeInsets edgeInsetsOnly({double left: 0.0, double top: 0.0, double right: 0.0, double bottom: 0.0}) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(left.toInt()),
        right: ScreenUtil().setWidth(right.toInt()),
        top: ScreenUtil().setHeight(top.toInt()),
        bottom: ScreenUtil().setHeight(bottom.toInt())
      );
    }
    static EdgeInsets edgeInsetsfromLTRB(double left, double top, double right, double bottom) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(left.toInt()),
        right: ScreenUtil().setWidth(right.toInt()),
        top: ScreenUtil().setHeight(top.toInt()),
        bottom: ScreenUtil().setHeight(bottom.toInt())
      );
    }
    static EdgeInsets margin(double left, double top, double right, double bottom) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(left.toInt()),
        right: ScreenUtil().setWidth(right.toInt()),
        top: ScreenUtil().setHeight(top.toInt()),
        bottom: ScreenUtil().setHeight(bottom.toInt())
      );
    }
    static EdgeInsets marginLeft(double left) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(left.toInt())
      );
    }
    static EdgeInsets marginRight(double right) { 
      return EdgeInsets.only(
        right: ScreenUtil().setWidth(right.toInt())
      );
    }
    static EdgeInsets marginTop(double top) { 
      return EdgeInsets.only(
        top: ScreenUtil().setHeight(top.toInt())
      );
    }
    static EdgeInsets marginBottom(double bottom) { 
      return EdgeInsets.only(
        bottom: ScreenUtil().setHeight(bottom.toInt())
      );
    }
    static EdgeInsets padding(double left, double top, double right, double bottom) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(left.toInt()),
        right: ScreenUtil().setWidth(right.toInt()),
        top: ScreenUtil().setHeight(top.toInt()),
        bottom: ScreenUtil().setHeight(bottom.toInt())
      );
    }
    static EdgeInsets paddingLeft(double left) { 
      return EdgeInsets.only(
        left: ScreenUtil().setWidth(left.toInt())
      );
    }
    static EdgeInsets paddingRight(double right) { 
      return EdgeInsets.only(
        right: ScreenUtil().setWidth(right.toInt())
      );
    }
    static EdgeInsets paddingTop(double top) { 
      return EdgeInsets.only(
        top: ScreenUtil().setHeight(top.toInt())
      );
    }
    static EdgeInsets paddingBottom(double bottom) { 
      return EdgeInsets.only(
        bottom: ScreenUtil().setHeight(bottom.toInt())
      );
    }
    static fontSize(double size) { 
      return ScreenUtil().setSp(size.toInt());
    }
    static width(double width) { 
      return ScreenUtil().setWidth(width.toInt());
    }
    static height(double height) { 
      return ScreenUtil().setHeight(height.toInt());
    }
}